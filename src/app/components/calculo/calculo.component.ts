import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculo',
  templateUrl: './calculo.component.html',
  styleUrls: ['./calculo.component.css']
})
export class CalculoComponent implements OnInit {

  var1: number = 50;
  var2: number = 20;
  suma: number;

  constructor() { 
    this.suma = this.var1 + this.var2;
  }

  ngOnInit(): void {
  }
}
